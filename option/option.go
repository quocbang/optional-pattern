package option

import "log"

type option struct {
	isMigrate bool
	database  []string
}

type Options func(*option)

func MaybeMigrate() Options {
	return func(o *option) {
		o.isMigrate = true
	}
}

func MigrateDatabase(databases []string) Options {
	return func(o *option) {
		o.database = databases
	}
}

func DoOption(opts ...Options) {
	options := &option{}
	for _, o := range opts {
		o(options)
	}
	log.Println(options)
}
